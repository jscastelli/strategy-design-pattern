#ifndef SPACEDUCK_HPP
#define SPACEDUCK_HPP

#include "Duck.hpp"

/* Space duck is a concrete class that inherits from Duck base class*/
class SpaceDuck : public Duck
{
   public: 
      SpaceDuck();
};
#endif