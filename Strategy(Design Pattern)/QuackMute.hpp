#ifndef QUACKMUTE_HPP
#define QUACKMUTE_HP

#include "QuackBehaviorInterface.hpp"

/* QuackMute is a concrete class that inherits from QuackBehaviorInterface
 * and provides functionality(or lack thereof) for objects that have the 
 * quack behavior.
*/
class QuackMute : public QuackBehaviorInterface
{
   void Quack() override;
};
#endif