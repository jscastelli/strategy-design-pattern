/*  Created by: Joshua Castelli
 *        Date: 1/23/2020
 * Description: Strategy design pattern example based ont he Heads Up Design
 *              Patterns book by Eric Freeman & Elisabeth Robson.
*/

/* Strategy design pattern */
 
/* The Strategy design pattern defines a family of algorithms
 * (concrete behaviors in this case), encapsulates each one, and makes them 
 * interchangable. Strategy lets the algorithm vary independantly from clients
 * that use it. 
*/

#include <iostream>
#include "Duck.hpp"
#include "Mallard.hpp"
#include "RubberDuck.hpp"
#include "SpaceDuck.hpp"
#include "FlyWithWings.hpp"
#include "FlyWithRocket.hpp"
#include "FlyNoFly.hpp"
#include "QuackMute.hpp"
#include "QuackQuack.hpp"
#include "QuackSqueak.hpp"

int main()
{
   /* DUCK BASE CLASS */
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   std::cout << "DUCK:" << std::endl;
   Duck* duck = new Duck();
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Change Duck to FlyNoFly and QuackMute:" << std::endl;
   duck->ChangeFlyBehavior(new FlyNoFly());
   duck->ChangeQuackBehavior(new QuackMute());
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Deleting Duck:" << std::endl;
   delete duck;
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   /* END DUCK */

   /* MALLARD */
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   std::cout << "MALLARD:" << std::endl;
   duck = new Mallard();
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Change Mallard to FlyNoFly and QuackMute:" << std::endl;
   duck->ChangeFlyBehavior(new FlyNoFly());
   duck->ChangeQuackBehavior(new QuackMute());
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Deleting Mallard:" << std::endl;
   delete duck;
   std::cout<< "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   /* END MALLARD */

   /* RUBBER DUCK */
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   std::cout << "RUBBER DUCK:" << std::endl;
   duck = new RubberDuck();
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Change Rubber Duck to FlyWithRocket and QuackQuack:" << std::endl;
   duck->ChangeFlyBehavior(new FlyWithRocket());
   duck->ChangeQuackBehavior(new QuackQuack());
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Deleting Rubber Duck:" << std::endl;
   delete duck;
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   /* END RUBBER DUCK*/

   /* SPACE DUCK */
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   std::cout << "SPACE DUCK:" << std::endl;
   duck = new SpaceDuck();
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Change Space Duck to FlyWithWings and QuackSqueak:" << std::endl;
   duck->ChangeFlyBehavior(new FlyWithWings);
   duck->ChangeQuackBehavior(new QuackSqueak());
   std::cout << "     Fly: ";
   duck->PerformFlyBehavior();
   std::cout << "   Quack: ";
   duck->PerformQuackBehavior();
   std::cout << std::endl;

   std::cout << "Deleting Space Duck:" << std::endl;
   delete duck;
   std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
   /* END SPACE DUCK */

   system("pause");
}