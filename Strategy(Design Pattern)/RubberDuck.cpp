#include "RubberDuck.hpp"
#include "FlyNoFly.hpp"
#include "QuackSqueak.hpp"

//The constructor initializes Rubber Duck specific behavior for fly and Quack
RubberDuck::RubberDuck()
{
   //NOTE: you could NOT initialize mFlyBehavior because Duck base class
   //accounts for uninitialzied behavior. But in this case, we will for
   //learning purposes.
   mFlyBehavior = new FlyNoFly();      //Rubber ducks don't fly
   mQuackBehavior = new QuackSqueak(); //Rubber ducks squeak
}
