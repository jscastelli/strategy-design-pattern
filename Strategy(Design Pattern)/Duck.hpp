#ifndef DUCK_HPP
#define DUCK_HPP

#include "FlyBehaviorInterface.hpp"
#include "QuackBehaviorInterface.hpp"

#include "FlyWithWings.hpp"
#include "QuackQuack.hpp"

/* Duck is a base class that will be inherited by concrete classes
 * (i.e. Mallard, Rubber Duck, etc.)
 */
class Duck
{
   public:

      //Constructor
      Duck() {};

      //Destructor
      ~Duck();


   /* ~~~~~ Member Functions ~~~~~ */

      //Fly function
      void PerformFlyBehavior();

      //Quack function
      void PerformQuackBehavior();

      //Change behaviors during runtime
      void ChangeFlyBehavior(FlyBehaviorInterface* newFlyBehavior);
      void ChangeQuackBehavior(QuackBehaviorInterface* newQuackBehavior);

   /* ~~~~~ End Member Functions ~~~~~ */
   /* ================================ */

   
   /* ~~~~~ Member Variables ~~~~~ */
   protected:
      //Fly behavior variable
      FlyBehaviorInterface* mFlyBehavior;

      //Quack behavior variable
      QuackBehaviorInterface* mQuackBehavior;

   /* ~~~~~ End Member Variables ~~~~~ */
   /* ================================ */
};
#endif