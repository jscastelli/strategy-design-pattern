#include "SpaceDuck.hpp"
#include "FlyWithRocket.hpp"
#include "QuackMute.hpp"

//The constructor initializes Space Duck specific behavior for fly and Quack
SpaceDuck::SpaceDuck()
{
   mFlyBehavior = new FlyWithRocket(); //Space ducks fly with rockets
   mQuackBehavior = new QuackMute();   //There is no sound in space!
}
