#include "Mallard.hpp"
#include "FlyWithWings.hpp"
#include "QuackQuack.hpp"

//The constructor initializes Mallard specific behavior for Fly and Quack
Mallard::Mallard()
{
   mFlyBehavior = new FlyWithWings(); //Mallards fly with wings
   mQuackBehavior = new QuackQuack(); //Mallard Quack normally
}
