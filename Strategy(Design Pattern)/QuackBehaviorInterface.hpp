#ifndef QUACKBEHAVIORINTERFACE_HPP
#define QUACKBEHAVIORINTERFACE_HPP

/* Quack Behavior is an abstract base class meant to be used as an interface
 * for different types of quacking behavior.
*/
class QuackBehaviorInterface
{
   public:

      /* Quack must be defined in concrete behavior classes(i.e. QuackSqueak),
       * that inherit this interface, as it is a pure virtual function.
       * This allows for flexibility in changing the behavior of Quack based
       * on the object it is associated with(i.e. Rubber Ducks don't Quack,
       * they squeak, therefore Quack will have squeaking functionality. 
       * There is no sound in space, therefore Space ducks will have no
       * quacking functionality).
      */
      virtual void Quack() = 0;
};
#endif