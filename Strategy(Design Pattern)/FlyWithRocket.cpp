#include "FlyWithRocket.hpp"
#include <iostream>

//Fly provides the functionality(with rockets) for flying behavior.
void FlyWithRocket::Fly()
{
   std::cout << "Rocket Power!" << std::endl;
}
