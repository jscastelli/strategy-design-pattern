#include "QuackMute.hpp"
#include <iostream>

//Quack provides the functionality(or lack thereof) for quacking behavior.
void QuackMute::Quack()
{
   std::cout << "I don't quack!" << std::endl;
}
