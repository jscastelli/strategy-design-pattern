#include "QuackSqueak.hpp"
#include <iostream>

//Quack provides the functionality(squeak) for quacking behavior.
void QuackSqueak::Quack()
{
   std::cout << "Squeak Squeak!" << std::endl;
}
