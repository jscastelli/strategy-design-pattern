#include "FlyNoFly.hpp"
#include <iostream>

//Fly provides the functionality(or lack thereof) for flying behavior.
void FlyNoFly::Fly()
{
   std::cout << "I can't fly!" << std::endl;
}
