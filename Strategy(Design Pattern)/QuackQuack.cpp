#include "QuackQuack.hpp"
#include <iostream>

//Quack provides the functionality(normal quack) for quacking behavior.
void QuackQuack::Quack()
{
   std::cout << "Quack Quack!" << std::endl;
}
