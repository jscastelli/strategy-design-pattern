#ifndef FLYBEHAVIORINTERFACE_HPP
#define FLYBEHAVIORINTERFACE_HPP

/* Fly Behavior is an abstract base class meant to be used as an interface
 * for different types of flying behavior.
*/
class FlyBehaviorInterface
{
   public:

      /* Fly must be defined in concrete behavior classes(i.e. FlyWithWings),
       * that inherit this interface, as it is a pure virtual function.
       * This allows for flexibility in changing the behavior of fly based
       * on the object it is associated with(i.e. Rubber Ducks don't fly, 
       * therefore Fly will have no functionality. Space ducks use rockets
       * to fly and therefore Fly will have functionality involving rockets).
      */
      virtual void Fly() = 0; 
};
#endif