#ifndef MALLARD_HPP
#define MALLARD_HPP

#include "Duck.hpp"

/* Mallard is a concrete class that inherits from Duck base class */
class Mallard : public Duck
{
   public:
      Mallard();
};
#endif