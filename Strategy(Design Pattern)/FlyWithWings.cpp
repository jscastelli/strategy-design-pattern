#include "FlyWithWings.hpp"
#include <iostream>

//Fly provides the functionality(with wings) for flying behavior.
void FlyWithWings::Fly()
{
   std::cout << "Flying with wings!" << std::endl;
}
