#include "Duck.hpp"
#include <iostream>

//Destructor cleans up pointer variables so there are no memory leaks
Duck::~Duck()
{
   if (mFlyBehavior)
   {
      delete mFlyBehavior;
      mFlyBehavior = nullptr;
      std::cout << "Fly behavior cleaned up" << std::endl;
   }
    
   if (mQuackBehavior)
   {
      delete mQuackBehavior;
      mFlyBehavior = nullptr;
      std::cout << "Quack behavior cleaned up" << std::endl;
   }
}

//Performs fly behavior if one is available.
void Duck::PerformFlyBehavior()
{
   if (mFlyBehavior)
   {
      mFlyBehavior->Fly();
   }
   else
   {
      std::cout << "This duck has no FLY behavior." << std::endl;
   }
}
//Performs quack behavior is one is available
void Duck::PerformQuackBehavior()
{
   if (mQuackBehavior)
   {
      mQuackBehavior->Quack();
   }
   else
   {
      std::cout << "This duck has no QUACK behavior." << std::endl;
   }
}

//Changes/sets the fly behavior
void Duck::ChangeFlyBehavior(FlyBehaviorInterface* newFlyBehavior)
{
   if (mFlyBehavior)
   {
      delete mFlyBehavior;
      mFlyBehavior = newFlyBehavior;
   }
   else
   {
      mFlyBehavior = newFlyBehavior;
   }
}

//Changes/sets the quack behavior
void Duck::ChangeQuackBehavior(QuackBehaviorInterface* newQuackBehavior)
{
   if (mQuackBehavior)
   {
      delete mQuackBehavior;
      mQuackBehavior = newQuackBehavior;
   }
   else
   {
      mQuackBehavior = newQuackBehavior;
   }
}
